from django.apps import AppConfig


class meal_plansConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "meal_plans"
