from django.contrib import admin

from meal_plans.models import Meal_plans

# Register your models here.
class Meal_plansAdmin(admin.ModelAdmin):
    pass


admin.site.register(Meal_plans, Meal_plansAdmin)
