from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from meal_plans.models import Meal_plans

# Create your views here.
class MealPlanListView(LoginRequiredMixin, ListView):
    model = Meal_plans
    template_name = "meal_plans/list.html"
    # paginated_by = 2
    def get_queryset(self):
        return Meal_plans.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = Meal_plans
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = Meal_plans
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return Meal_plans.objects.filter(owner=self.request.user)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_plans
    template_name = "meal_plans/edit.html"
    fields = ["name"]

    def get_queryset(self):
        return Meal_plans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_list", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_plans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return Meal_plans.objects.filter(owner=self.request.user)
