from django.urls import path

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)

urlpatterns = [
    path("meal_plans/", MealPlanListView.as_view(), name="meal_plans"),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="meal_plans_create",
    ),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plans_detail",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plans_edit",
    ),
]
